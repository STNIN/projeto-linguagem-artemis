package gerador;

import programa.Programa;
import programa.instrucao.Instrucao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class GeradorWASM {

    private Programa programa;

    private final static String URL_ARQUIVO = "tmp\\programaNew.wat";

    public GeradorWASM(Programa programa) {
        this.programa = programa;
    }

    public void gerar() {
        escreverArquivo("\n(module");
        Instrucao instrucao = programa.obterInstrucao();
        while (instrucao != null) {
            escreverArquivo(instrucao.obterInstrucao());
            instrucao = programa.obterInstrucao();
        }
        escreverArquivo("\n)");
    }


    private void escreverArquivo(String instrucoes) {

        try {
            FileWriter fileWriter = new FileWriter(URL_ARQUIVO,true);

            PrintWriter printWriter = new PrintWriter( fileWriter );

            printWriter.printf("%s",instrucoes);

            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
