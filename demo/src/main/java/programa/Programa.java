package programa;

import programa.instrucao.Instrucao;

import java.util.LinkedList;

public class Programa {
    private LinkedList<Instrucao> instrucoes;

    public Programa() {
        instrucoes = new LinkedList<>();
    }

    public void inserirInstrucao(Instrucao instrucao) {
        this.instrucoes.add(instrucao);
    }

    public Instrucao obterInstrucao() {
        return instrucoes.poll();
    }
}
