package programa.modelo.operacao;

import exception.TipoNaoEncontradoException;

import java.util.Arrays;

public enum TipoOperacao {
    SOMA("add", "+"),
    SUBTRACAO("sub", "-"),
    MULTIPLICACAO("mul","*"),
    DIVISAO("div_s","/");

    private String valor;
    private String valorArtemis;

    TipoOperacao( String valor, String valorArtemis ) { this.valor = valor; this.valorArtemis = valorArtemis; }

    public String getValor() { return this.valor; }

    public static TipoOperacao construirTipoOperacao(String tipo ) {
        return Arrays.stream( TipoOperacao.values() )
                .filter( tipoAtual -> tipoAtual.valorArtemis.equals(tipo))
                .findFirst()
                .orElseThrow( () -> new TipoNaoEncontradoException(String.format("Error - Tipo [%s] nao Encontrado",tipo)));
    }
}
