package programa.instrucao.operacao;

import programa.instrucao.Instrucao;
import programa.modelo.operacao.TipoOperacao;
import programa.modelo.variavel.Variavel;

public class OperacaoAritmetica implements Instrucao {

    private Variavel destino;
    private Variavel operando1;
    private Variavel operando2;
    private TipoOperacao operador;

    public OperacaoAritmetica( Variavel destino,Variavel operando1, Variavel operando2, TipoOperacao operador) {
        this.destino = destino;
        this.operando1 = operando1;
        this.operando2 = operando2;
        this.operador = operador;
    }

    @Override
    public String obterInstrucao() {
        return String.format("\n(local.set $%s (%s.%s (local.get $%s) (local.get $%s) ) )",
                destino.getNome(),
                destino.getTipoVariavel().getValor(),
                operador.getValor(),
                operando1.getNome(),
                operando2.getNome());
    }
}
