package gerado;// Generated from C:/Users/Guilherme Neves/Downloads/demo/src/main/java\Artemis.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ArtemisParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ArtemisVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#artemis}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArtemis(ArtemisParser.ArtemisContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#importRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitImportRule(ArtemisParser.ImportRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#externImport}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExternImport(ArtemisParser.ExternImportContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#struct}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct(ArtemisParser.StructContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#mainFunction}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMainFunction(ArtemisParser.MainFunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#functions}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctions(ArtemisParser.FunctionsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#functionCall}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall(ArtemisParser.FunctionCallContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#parametersDefinition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParametersDefinition(ArtemisParser.ParametersDefinitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#parametersPassing}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParametersPassing(ArtemisParser.ParametersPassingContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#returnRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnRule(ArtemisParser.ReturnRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#escope}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEscope(ArtemisParser.EscopeContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#conditionals}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionals(ArtemisParser.ConditionalsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#ifRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIfRule(ArtemisParser.IfRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#elseIfRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseIfRule(ArtemisParser.ElseIfRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#elseRule}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitElseRule(ArtemisParser.ElseRuleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#whileLoop}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhileLoop(ArtemisParser.WhileLoopContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(ArtemisParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#intExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntExpression(ArtemisParser.IntExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#floatExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatExpression(ArtemisParser.FloatExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#charExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharExpression(ArtemisParser.CharExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#stringExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringExpression(ArtemisParser.StringExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#booleanExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanExpression(ArtemisParser.BooleanExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#intValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntValues(ArtemisParser.IntValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#floatValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatValues(ArtemisParser.FloatValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#charValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharValues(ArtemisParser.CharValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#stringValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringValues(ArtemisParser.StringValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#booleanValuesExpression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanValuesExpression(ArtemisParser.BooleanValuesExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#allOperators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAllOperators(ArtemisParser.AllOperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#logicRelationaOperators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicRelationaOperators(ArtemisParser.LogicRelationaOperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#sumOperators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSumOperators(ArtemisParser.SumOperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#multiplicationOperators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicationOperators(ArtemisParser.MultiplicationOperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#logicOperators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogicOperators(ArtemisParser.LogicOperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#relationalOperators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalOperators(ArtemisParser.RelationalOperatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#globalVariable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGlobalVariable(ArtemisParser.GlobalVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#localVariable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLocalVariable(ArtemisParser.LocalVariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#variableDeclaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableDeclaration(ArtemisParser.VariableDeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#variableAssigner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariableAssigner(ArtemisParser.VariableAssignerContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOperation(ArtemisParser.OperationContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#structFieldAssigner}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructFieldAssigner(ArtemisParser.StructFieldAssignerContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#typeAssignerValue}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypeAssignerValue(ArtemisParser.TypeAssignerValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#types}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypes(ArtemisParser.TypesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#values}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValues(ArtemisParser.ValuesContext ctx);
	/**
	 * Visit a parse tree produced by {@link ArtemisParser#booleanValues}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanValues(ArtemisParser.BooleanValuesContext ctx);
}