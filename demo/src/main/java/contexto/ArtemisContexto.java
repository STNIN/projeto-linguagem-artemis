package contexto;

import gerado.ArtemisBaseListener;
import gerado.ArtemisParser;
import gerador.GeradorWASM;
import programa.instrucao.Instrucao;
import programa.instrucao.biblioteca.GerenciadorDeBiblioteca;
import programa.instrucao.biblioteca.io.Impressor;
import programa.instrucao.variavel.AtribuicaoValor;
import programa.instrucao.funcao.DeclaracaoFuncao;
import programa.instrucao.variavel.DeclaracaoVariavel;
import programa.instrucao.operacao.Operacao;

public class ArtemisContexto extends ArtemisBaseListener {

    private programa.Programa prog;
    private DeclaracaoFuncao func;

    public ArtemisContexto(){
        prog = new programa.Programa();
    }

    @Override
    public void enterArtemis(ArtemisParser.ArtemisContext ctx) {

        System.out.println(String.format("[START GRAMMAR] ----------> EnterArtemis\n"));

        super.enterArtemis(ctx);
    }

    @Override
    public void exitArtemis(ArtemisParser.ArtemisContext ctx) {

        System.out.println(String.format("[END GRAMMAR] ExitArtemis <----------"));

        GeradorWASM geradorWasm = new GeradorWASM( prog );

        geradorWasm.gerar();

        super.exitArtemis(ctx);
    }

    @Override
    public void enterExternImport(ArtemisParser.ExternImportContext ctx) {

        String libraryName = ctx.getImportName.getText();

        GerenciadorDeBiblioteca gen = new GerenciadorDeBiblioteca();
        Instrucao instrucao = gen.obterLib(libraryName);
        prog.inserirInstrucao(instrucao);

        System.out.println(String.format("[NEW] --> EnterExternImport - Import <<%s>> Created",libraryName));

        super.enterExternImport(ctx);
    }

    @Override
    public void exitExternImport(ArtemisParser.ExternImportContext ctx) {

        System.out.println(String.format("ExitExternImport - Import Finished\n"));

        super.exitExternImport(ctx);
    }

    @Override
    public void enterMainFunction(ArtemisParser.MainFunctionContext ctx) {

        String nomeFuncao = ctx.getFunctionName.getText();

        func = new DeclaracaoFuncao(nomeFuncao);

        System.out.println(String.format("[NEW] --> EnterMainFunction - Function <<%s>> Created",nomeFuncao));

        super.enterMainFunction(ctx);
    }

    @Override
    public void exitMainFunction(ArtemisParser.MainFunctionContext ctx) {

        System.out.println(String.format("EnterMainFunction - Function Finished\n"));

        prog.inserirInstrucao(func);

        super.exitMainFunction(ctx);
    }

    @Override
    public void enterFunctions(ArtemisParser.FunctionsContext ctx) { super.enterFunctions(ctx); }

    @Override
    public void exitFunctions(ArtemisParser.FunctionsContext ctx) {

        System.out.println(String.format("EnterFunction - Function Finished\n"));

        super.exitFunctions(ctx);
    }

    @Override
    public void enterFunctionCall(ArtemisParser.FunctionCallContext ctx) {

        String nomeFuncao = ctx.getFunctionName.getText();
        String param = ctx.getParameterPassing.getText();

        System.out.println(String.format("\tEnterFunctionCall - Function <<%s>> Called",nomeFuncao));

        Impressor imp = new Impressor(param);

        func.inserirInstrucao(imp);

        super.enterFunctionCall(ctx);
    }

    @Override
    public void exitFunctionCall(ArtemisParser.FunctionCallContext ctx) {

        System.out.println(String.format("\tExitFunctionCall - Function called Finishedc"));

        super.exitFunctionCall(ctx);
    }

    @Override
    public void enterOperation(ArtemisParser.OperationContext ctx) {
        Operacao operacao = new Operacao(ctx.getVariableDestName.getText(),ctx.getOpt1Name.getText(),ctx.getOpt2Name.getText(),ctx.getOperator.getText());
        func.inserirInstrucao(operacao);
        super.enterOperation(ctx);
    }

    @Override
    public void enterLocalVariable(ArtemisParser.LocalVariableContext ctx) {

        String nomeVariavel = ctx.getLocalVariableName.getText();
        String tipoVariavel = ctx.getTypeAssignerValue.getText().replaceAll("<-.*", "");
        String valorVariavel = ctx.getTypeAssignerValue.getText().replaceAll(".*<-", "");

        DeclaracaoVariavel dec = DeclaracaoVariavel.declararVariavel(nomeVariavel,tipoVariavel);

        func.inserirInstrucao(dec);

        AtribuicaoValor atribuicaoValor = new AtribuicaoValor(dec.getVariavel());

        atribuicaoValor.atribuirValor(valorVariavel);

        func.inserirInstrucao(atribuicaoValor);

        System.out.println(String.format("\t[NEW] --> EnterLocalVariable - Local Variable <<%s>> Created", nomeVariavel));


        super.enterLocalVariable(ctx);
    }

    @Override
    public void exitLocalVariable(ArtemisParser.LocalVariableContext ctx) {

        System.out.println(String.format("\tEnterLocalVariable - Local Variable Finished"));

        super.exitLocalVariable(ctx);
    }

    @Override
    public void enterVariableAssigner(ArtemisParser.VariableAssignerContext ctx) {

        String nomeVariavel = ctx.getVariableName.getText();
        String valorVariavel = ctx.getValues.getText();

        System.out.println(String.format("\tEnterVariableAssigner - Variable <<%s>> Assigned", nomeVariavel));

        super.enterVariableAssigner(ctx);
    }

    @Override
    public void exitVariableAssigner(ArtemisParser.VariableAssignerContext ctx) {

        System.out.println(String.format("\tEnterVariableAssigner - Variable Assigned Finished"));

        super.exitVariableAssigner(ctx);
    }

    @Override
    public void enterVariableDeclaration(ArtemisParser.VariableDeclarationContext ctx) {

        String nomeVariavel = ctx.getLocalVariableName.getText();
        String tipoVariavel = ctx.getVariableTypes.getText();

        System.out.println(String.format("\t[NEW] --> EnterVariableDeclaration - Local Variable <<%s>> Created", nomeVariavel));

        super.enterVariableDeclaration(ctx);
    }

    @Override
    public void exitVariableDeclaration(ArtemisParser.VariableDeclarationContext ctx) {

        System.out.println(String.format("\tEnterVariableDeclaration - Local Variable Finished"));

        super.exitVariableDeclaration(ctx);
    }
}
