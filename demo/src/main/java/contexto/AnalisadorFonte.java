package contexto;

import gerado.ArtemisLexer;
import gerado.ArtemisParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;

public class AnalisadorFonte {

    private ArtemisContexto listener;

    public void analisar(String fonte) {

        try {

            ArtemisLexer lexico = new ArtemisLexer(CharStreams.fromFileName(fonte));

            CommonTokenStream tokens = new CommonTokenStream(lexico);

            ArtemisParser sintatico = new ArtemisParser(tokens);

            ArtemisParser.ArtemisContext arvore = sintatico.artemis();

            ParseTreeWalker verificador = new ParseTreeWalker();

            listener = new ArtemisContexto();

            verificador.walk(listener, arvore);

        } catch (IOException ex) {

            System.out.println("FATAL: não achei o fonte " + fonte);

            System.exit(1);

        }
    }
}