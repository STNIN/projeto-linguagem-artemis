package exception;

public class OperadorNaoEncontradoException extends RuntimeException{

    public OperadorNaoEncontradoException( String mensagem ){
        super(mensagem);
    }
}
