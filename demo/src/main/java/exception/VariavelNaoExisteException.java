package exception;

public class VariavelNaoExisteException extends RuntimeException {

    public VariavelNaoExisteException(String mensagem ){
        super(mensagem);
    }

}
