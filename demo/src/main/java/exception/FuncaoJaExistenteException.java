package exception;

public class FuncaoJaExistenteException extends RuntimeException {

    public FuncaoJaExistenteException( String mensagem ){
        super(mensagem);
    }
}
